# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 17:46:08 2020

@author: cheta
"""

import argparse
import csv
from operator import itemgetter
from datetime import datetime
import itertools

def parse_args():
    """Parses arguments passed in the shell to be used in the main function.
    Returns:
        args -- arguments
    """
    parser = argparse.ArgumentParser(description='Look for Consumer Complaint Analysis')
    parser.add_argument('--input', help="enter the input filename", type=str)
    parser.add_argument('--output', help="enter the output filename", type=str)
    args = parser.parse_args()
    return args

#read input csv file
def read_csv(input_file_path):
    complaints = []
    #'/Consumer_Complaints/insight_testsuite/test_1/input/complaints.csv'
    with open(input_file_path, 'r', encoding="utf8") as entry_data:
        my_reader = csv.reader(entry_data, delimiter=',')
        for row in my_reader:
            complaints.append(row)
    return complaints

#lower case the words
def lower_case(words):
    out = ''
    for n in words:
        if n not in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
            out = out + n
        else:
            k = ord(n)
            l = k + 32
            out = out + chr(l)
    return out

#lowercase Product and companies name, and also extract the year from 'Date recieved' value
def prepare_data(consumer_complaints):
    for complaint in consumer_complaints[1:]:
        complaint[7] = lower_case(complaint[7])
        complaint[1] = lower_case(complaint[1])
        complaint[0] = datetime.strptime(complaint[0],'%Y-%m-%d').year
    return consumer_complaints[1:]

def write_output_file(output_file_path, final_output_list):
#    '/Consumer_Complaints/output/report.csv'
    with open(output_file_path, 'w') as report_file:
        #wr = csv.writer(report_file)
        #wr.writerows(final_output_list)
        outfile_writer = csv.writer(report_file, delimiter=',', quotechar='"',
                                    quoting=csv.QUOTE_MINIMAL)

        # for each row in the final list, remove the list of list
        # and create one list
        for row in final_output_list:
            outfile_writer.writerow(row)

def main():
    print("This is main function")
    
     # Input and Output files Error-Handling
    args = parse_args()
    if args.input is None:
        raise ImportError('Did not specify the correct input file!')
    if args.output is None:
        raise ImportError('Did not specify the correct output file!')
    
    #read consumer complaints csv file
    consumer_complaints = read_csv(args.input)
    
    final_sorted_list = prepare_data(consumer_complaints)
    
    def sort_by_product_year(array_element): 
        return (array_element[0], array_element[1])
    
    #Sort by both product and year
    final_sorted_list.sort(key=sort_by_product_year)
    
    
    prev_iteration_length = 0
    final_output_list = []
    for year_product_tup, g in itertools.groupby(final_sorted_list, key=lambda x:(x[0], x[1])):
        frequency = len(list(g))
        temp_array = []
        year, product = year_product_tup
        
        temp_array.append(product)
        temp_array.append(year)
        temp_array.append(frequency)
        
        end = prev_iteration_length + frequency
        iterations = range(prev_iteration_length, end)
        prev_iteration_length = end
        
        unique_companies = set()
        for i in iterations:
            if final_sorted_list[i][0] == year and final_sorted_list[i][1] == product:
                unique_companies.add(final_sorted_list[i][7])
        
        temp_array.append(len(unique_companies))
        temp_array.append(int(round((temp_array[3] / temp_array[2])*100)))
        unique_companies = set()
        final_output_list.append(temp_array)

    
    #sort list chronologically
    final_output_list.sort(key = lambda product: product[0], reverse = False)
    
    #write output data to csv file
    write_output_file(args.output, final_output_list)

if __name__ == '__main__':
    main()